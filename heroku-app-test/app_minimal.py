import pandas as pd

from flask import Flask, make_response, jsonify, request, render_template
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

# reading data
final_results = pd.read_csv('./model_results.csv')

### basic route ###
@app.route('/')
def hello():
    return 'hello!'

@app.route('/get_model_results')
def get_model_result():
    return final_results.to_json(orient='split', index=False)

