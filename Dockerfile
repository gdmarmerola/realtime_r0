ARG BASE_CONTAINER=jupyter/minimal-notebook
FROM $BASE_CONTAINER

COPY . /realtime_r0

WORKDIR /realtime_r0

RUN pip install -r requirements.txt