# importing core 
# - several modeling and data functions there
import io
import json
import base64
import numpy as np
import pandas as pd
from core import run_full_model, load_data, plot_rt, plot_standings
from matplotlib import pyplot as plt

# import joblib for parallel computation
from joblib import Parallel, delayed

# FigureCanvas do send plot over API
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas

# importing flask
from flask import Flask, make_response, jsonify, request, render_template

app = Flask(__name__)

### parameters ###

# number of cores to parallelize posterior estimation
N_JOBS = -1

### reading data ###

# reading from sources (wcota)
city_df, state_df = load_data()

# using only new case data
city_df = city_df['confirmed_new']
state_df = state_df['confirmed_new']

def update_model():

    with Parallel(n_jobs=N_JOBS) as parallel:
        results = parallel(delayed(run_full_model)(grp[1]) for grp in state_df.groupby(level='state'))

    global final_results
    final_results = pd.concat(results)
    
#update_model()

### basic route ###
@app.route('/')
def hello():
    return 'hello!'

### update data ###
@app.route('/update_data')
def update_data():

    # making global variables
    global city_df
    global state_df

    # reading from sources (wcota)
    city_df, state_df = load_data()

    # using only new case data
    city_df = city_df['confirmed_new']
    state_df = state_df['confirmed_new']

    return 'done'

### checking last date ###

@app.route('/check_last_date')
def check_last_date():

    last_date_state = str(state_df.index.get_level_values(1).max().date())
    last_date_city = str(city_df.index.get_level_values(1).max().date())

    return json.dumps({'last_date_state':last_date_state,
                       'last_date_city':last_date_city})


@app.route('/get_state_list')
def get_state_list():

    return pd.Series(state_df.index.get_level_values(0).unique()).to_json()

### running posteriors for specific state ###

@app.route('/get_model_result_state')
def get_model_result_state():

    state_name = request.args.get('state')

    this_state = state_df.loc[lambda x: x.index.get_level_values(0) == state_name]

    results = run_full_model(this_state)

    return results.reset_index().to_json(orient='split', index=False)

### running all posteriors ###

@app.route('/update_model')
def update_model_endpoint():
    
    update_model() 

    return 'done'

### sending all data over ###

@app.route('/get_model_result')
def get_model_result():
    try:
        return final_results.reset_index().to_json(orient='split', index=False)
    except:
        return 'model_not_yet_calculated'

### plotting ###

@app.route('/plot_rt_time')
def plot_rt_time():
    
    try:

        # number of columns and rows for plotting
        N_COLS = 4
        N_ROWS = int(np.ceil(len(results) / N_COLS))

        # opening figura
        fig, axes = plt.subplots(nrows=N_ROWS, ncols=N_COLS, figsize=(15, N_ROWS*3), dpi=90)

        # loop for several states
        for i, (state_name, result) in enumerate(final_results.groupby('state')):
            plot_rt(result, axes.flat[i], state_name)

        # saving figure
        fig.tight_layout()
        fig.set_facecolor('w')

        # Convert plot to PNG image
        pngImage = io.BytesIO()
        FigureCanvas(fig).print_png(pngImage)
        
        # Encode PNG image to base64 string
        pngImageB64String = "data:image/png;base64,"
        pngImageB64String += base64.b64encode(pngImage.getvalue()).decode('utf8')
        
        return render_template("image.html", image=pngImageB64String)

    except Exception as e:
        return print(e)


@app.route('/plot_states_comparison')
def plot_states_comparison():
    try:
        mr = final_results.groupby(level=0)[['ML', 'High_90', 'Low_90']].last()
        mr.sort_values('ML', inplace=True)
        fig, ax = plot_standings(mr, figsize=(13,5));

        # Convert plot to PNG image
        pngImage = io.BytesIO()
        FigureCanvas(fig).print_png(pngImage)
        
        # Encode PNG image to base64 string
        pngImageB64String = "data:image/png;base64,"
        pngImageB64String += base64.b64encode(pngImage.getvalue()).decode('utf8')
        
        return render_template("image.html", image=pngImageB64String)

    except Exception as e:
        return print(e)

if __name__ == '__main__':
    app.run(debug=True)









